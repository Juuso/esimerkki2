﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApplication1
{
    class Date
    {
        int day;
        int month;
        int year;

        public Date(int day1, int month1, int year1)
        {
            day = day1;
            month = month1;
            year = year1;
        }

        public int getDay()
        {
            return day;
        }

        public int getMonth()
        {
            return month;
        }

        public int getYear()
        {
            return year;
        }

        public void setDay(int day1)
        {
            day = day1;
        }

        public void setMonth(int month1)
        {
            month = month1;
        }

        public void setYear(int year1)
        {
            year = year1;
        }

        public void setDate(int day1, int month1, int year1)
        {
            day = day1;
            month = month1;
            year = year1;
        }

        public string toString()
        {
            return day + "," + month + "," + year + "";
        }

    }

}
